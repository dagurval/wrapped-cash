# Wrapped BCH contract and frontend

Wrapped BCH (WBCH) represents an equal amount of Bitcoin Cash (BCH), transforming it into a cashtoken format on the Bitcoin Cash blockchain. This enables BCH to be used as a cashtoken in smart contracts and decentralized applications on the Bitcoin Cash blockchain.

Contract is MIT licensed. Front-end is based on [Cash Ninjas](https://ninjas.cash/) front-end, so their license of picking.

