// Configure minting params

// Wallet Connect projectId
const projectId = "a84c402c36f2874bf0218d7e0f67d7c6";

// Url of the API server
const urlApiServer = "https://api.ninjas.cash";

// Contract Params mint
const tokenId = "ff4d6e4b90aa8158d39c5dc874fd9411af1ac3b5ed6f354755e8362a0d02c6b3";
const collectionSize = 5_000;
const numberOfThreads = 25;
const mintPriceSats = 5_000_000;
const payoutAddress = "bitcoincash:qqds0h006djrnast7ktvf7y3lrmvu0d7yqzhuzgvaa"; // with bitcoincash: or bchtest: prefix
const network = "mainnet";

// Wallet Connect Metadata
const wcMetadata = {
  name: 'Wrap-BCH',
  description: 'Wrap/unwrap BCH to cashtokens',
  url: 'https://wrapped.cash/',
  icons: ['https://wrapped.cash/images/logo.png']
};

export { projectId, urlApiServer, tokenId, collectionSize, mintPriceSats, payoutAddress, numberOfThreads, network, wcMetadata };