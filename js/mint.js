import SignClient from '@walletconnect/sign-client';
import { WalletConnectModal } from '@walletconnect/modal';
import { Contract, ElectrumNetworkProvider, SignatureTemplate } from "cashscript";
import contractArtifact from "/js/wrapbch.json";
import { decodeCashAddress, hexToBin, encodeCashAddress, decodeTransaction, cashAddressToLockingBytecode, stringify } from "@bitauth/libauth";
import { ElectrumCluster, ElectrumTransport } from 'electrum-cash';
import { projectId, urlApiServer, tokenId, network, wcMetadata } from "/js/mintingParams.js";

// 1. Setup Client with relay server
const signClient = await SignClient.init({
  projectId,
  // optional parameters
  relayUrl: 'wss://relay.walletconnect.com',
  metadata: wcMetadata
});

// Get last WalletConnect session from local storage is there is any
const lastKeyIndex = signClient.session.getAll().length - 1;
const lastSession = signClient.session.getAll()[lastKeyIndex];

// Handle session events
signClient.on('session_event', ({ event }) => {
  console.log('session_event');
  console.log(event);
});

signClient.on('session_update', ({ topic, params }) => {
  console.log('session_update');
  console.log(params);
});

signClient.on('session_delete', () => {
  console.log('session_delete');
});

// Connect Client.
const walletConnectModal = new WalletConnectModal({
  projectId: projectId,
  themeMode: 'dark',
  themeVariables: {
    '--wcm-background-color': '#20c997',
    '--wcm-accent-color': '#20c997',
  },
  explorerExcludedWalletIds: 'ALL',
});

const connectedChain = network == "mainnet" ? "bch:bitcoincash" : "bch:bchtest";
const requiredNamespaces = {
  bch: {
    chains: [connectedChain],
    methods: ['bch_getAddresses', 'bch_signTransaction', 'bch_signMessage'],
    events: ['addressesChanged'],
  },
};

let session;
if (lastSession) setTimeout(() => {
  const confirmReuse = confirm("The minting app is going to re-use your previous WalletConnect session");
  if (confirmReuse) {
    session = lastSession;
    document.getElementById('my-button').style.display = "none";
    document.getElementById('connectInfo').style.display = "none";
    document.getElementById('mintSection').style.display = "block";
  }
}, 500);

try {
  document.getElementById('my-button').addEventListener('click', async () => {
    const { uri, approval } = await signClient.connect({ requiredNamespaces });
    console.log(uri);
    if (session) return;
    await walletConnectModal.openModal({ uri });
    // Await session approval from the wallet.
    session = await approval();
    // Handle the returned session (e.g. update UI to "connected" state).
    document.getElementById('my-button').style.display = "none";
    document.getElementById('connectInfo').style.display = "none";
    document.getElementById('mintSection').style.display = "block";
    console.log(session);
    //onSessionConnect(session)
    // Close the QRCode modal in case it was open.
    walletConnectModal.closeModal();
  });

} catch (error) { console.log(error); }

// Create a custom 1-of-1 electrum cluster for bch-mainnet
const electrumCluster = new ElectrumCluster('Wrap-Cash', '1.4.1', 1, 1);
electrumCluster.addServer('fulcrum.greyh.at', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
const electrum = network == "mainnet" ? electrumCluster : undefined;
// Initialise cashscript ElectrumNetworkProvider
const electrumServer = new ElectrumNetworkProvider(network, electrum);

// Instantiate a new minting contract
const options = { provider: electrumServer };
const contract = new Contract(contractArtifact, [], options);
console.log(`P2sh32 smart contract address is ${contract.address}`);

const wrapButton = document.getElementById("wrapButton");
const wrapBCH = async() => {
  const bch = parseFloat(document.getElementById("quantity-nfts").value);
  try {
    if (isNaN(bch)) {
      throw ("Invalid number");
    }
    await performWrap(bch);
  }
  catch (e) {
    alert(e);
    cleanupFailedMint();
  }
}
wrapButton.onclick = wrapBCH;

const unwrapButton = document.getElementById("unwrapButton");
const unwrapBCH = async() => {
  const bch = parseFloat(document.getElementById("quantity-nfts").value);

  try {
    if (isNaN(bch)) {
      throw ("Invalid number");
    }
   await performUnwrap(parseFloat(bch));
  } catch (e) {
    alert(e);
    cleanupFailedMint();
  }
}
unwrapButton.onclick = unwrapBCH;

let nrsMinted = [];

// Cleanup state when mint fails. Fix button,
// and onclick callback.
const cleanupFailedMint = () => {
  wrapButton.textContent = "Wrap";
  wrapButton.onclick = wrapBCH;
  unwrapBCH.textContent = "Unwrap";
  unwrapButton.onclick = unwrapBCH;
};

const COIN = 100000000;
async function performWrap(mintTotal) {
  // Visual feedback for user, disable button onclick
  wrapButton.textContent = `Wrapping ${mintTotal} BCH...`;
  wrapButton.onclick = () => { };

  // Get userInput for mint
  const userAddress = await getUserAddress();
  const userUtxos = await electrumServer.getUtxos(userAddress);
  const networkFeeMint = 520;
  const mintPriceSats = mintTotal * COIN;
  const filteredUserUtxos = userUtxos.filter(
    val => !val.token && val.satoshis >= mintPriceSats + networkFeeMint,
  );
  const bchBalanceUser = userUtxos.reduce((total, utxo) => utxo.token ? total : total + utxo.satoshis, 0n);
  const userInput = filteredUserUtxos[0];
  if (!userInput && bchBalanceUser > BigInt(mintPriceSats + networkFeeMint)) {
    alert("No suitable utxos found for wrapping. You need to consolidate the balance of your utxos.");
    cleanupFailedMint();
    throw ("No suitable utxos found for wrapping. You need to consolidate the balance of your utxos.");
  }
  if (!userInput && bchBalanceUser <= BigInt(mintPriceSats)) {
    alert("No suitable utxos found for wrapping. Not enough funds in the wallet to mint!");
    cleanupFailedMint();
    throw ("No suitable utxos found for wrapping. Not enough funds in the wallet to mint!");
  }
  if (!userInput && bchBalanceUser <= BigInt(mintPriceSats + networkFeeMint)) {
    alert("No suitable utxos found for wrapping. Need enough BCH for the network fee!");
    cleanupFailedMint();
    throw ("No suitable utxos found for wrapping. Need enough BCH for the network fee!");
  }

  // Get the minting utxo to use from the api endpoint
  const getAvailableMintUtxo = async () => {
    const contractUtxos = await contract.getUtxos();
    console.log("Contract utxos", contractUtxos);

    let mintUtxo = contractUtxos.find(
      utxo => utxo.token != undefined && utxo.token.category == tokenId
    );
    return mintUtxo;
  };
  const selectedMintUtxo = await getAvailableMintUtxo();
  if (selectedMintUtxo === undefined) {
    alert("Did not find wrapping contract");
    cleanupFailedMint();
    throw ("Did not find wrapping contract");
  }
  // create minting transaction
  const newContractBCHBalance = selectedMintUtxo.satoshis + BigInt(mintPriceSats);
  const newContractTokenBalance = selectedMintUtxo.token.amount - BigInt(mintPriceSats);
  const tokenDetailsContract = {
    amount: newContractTokenBalance,
    category: tokenId,
  };
  const tokenDetailsUser = {
    amount: BigInt(mintPriceSats),
    category: tokenId,
  };
  const changeAmount = userInput.satoshis - BigInt(mintPriceSats) - 2000n;

  // empty usersig
  const userSig = new SignatureTemplate(Uint8Array.from(Array(32)));

  function toTokenAddress(address) {
    const addressInfo = decodeCashAddress(address);
    const pkhPayoutBin = addressInfo.payload;
    const prefix = network === "mainnet" ? 'bitcoincash' : 'bchtest';
    const tokenAddress = encodeCashAddress(prefix, "p2pkhWithTokens", pkhPayoutBin);
    return tokenAddress;
  }

  const transaction = contract.functions.wrapOrUnwrap()
    .from(selectedMintUtxo)
    .fromP2PKH(userInput, userSig)
    .to(contract.tokenAddress, newContractBCHBalance, tokenDetailsContract)
    .to(toTokenAddress(userAddress), 1000n, tokenDetailsUser)
    .withoutChange()
    .withTime(0);
  if (changeAmount > 1000n) transaction.to(userAddress, changeAmount);

  try {
    const rawTransactionHex = await transaction.build();
    console.log("Raw tx", rawTransactionHex);
    const decodedTransaction = decodeTransaction(hexToBin(rawTransactionHex));
    if (typeof decodedTransaction === "string") {
      alert("No suitable utxos found for minting. Try to consolidate your utxos!");
      throw ("No suitable utxos found for minting. Try to consolidate your utxos!");
    }
    decodedTransaction.inputs[1].unlockingBytecode = Uint8Array.from([]);

    // construct new transaction object for SourceOutputs, for stringify & not to mutate current network provider
    const listSourceOutputs = [{
      ...decodedTransaction.inputs[0],
      lockingBytecode: (cashAddressToLockingBytecode(contract.tokenAddress)).bytecode,
      valueSatoshis: BigInt(selectedMintUtxo.satoshis),
      token: selectedMintUtxo.token && {
        ...selectedMintUtxo.token,
        category: hexToBin(selectedMintUtxo.token.category),
      },
      contract: {
        abiFunction: transaction.abiFunction,
        redeemScript: contract.redeemScript,
        artifact: contract.artifact,
      }
    }, {
      ...decodedTransaction.inputs[1],
      lockingBytecode: (cashAddressToLockingBytecode(userAddress)).bytecode,
      valueSatoshis: BigInt(userInput.satoshis),
    }];

    const wcTransactionObj = {
      transaction: decodedTransaction,
      sourceOutputs: listSourceOutputs,
      broadcast: true,
      userPrompt: "Wrap BCH"
    };

    console.log(wcTransactionObj);
    setTimeout(() => alert('Approve the minting transaction in Cashonize'), 100);
    const signResult = await signTransaction(wcTransactionObj);
    console.log(signResult);
    if (signResult) {
      alert(`Wrap succesful! txid: ${signResult.signedTransactionHash}`);
      console.log(`Wrap succesful! txid: ${signResult.signedTransactionHash}`);
      wrapButton.textContent = "Wrap";

    } else {
      alert('Minting transaction cancelled');
      console.log('Minting transaction cancelled');
      wrapButton.textContent = "Wrap";
    }
    wrapButton.onclick = wrapBCH;
  } catch (error) {
    alert(error);
    console.log(error);
    cleanupFailedMint();
  }
}

async function performUnwrap(mintTotal) {
  // Visual feedback for user, disable button onclick
  unwrapButton.textContent = `Unwrapping ${mintTotal} BCH...`;
  unwrapButton.onclick = () => { };

  // Get userInput for mint
  const userAddress = await getUserAddress();
  const userUtxos = await electrumServer.getUtxos(userAddress);
  const networkFeeMint = 520;
  const mintPriceSats = mintTotal * COIN;
  const filteredBCHUserUtxos = userUtxos.filter(
    val => !val.token && val.satoshis >= networkFeeMint + 1000 /* token change */,
  );
  const filteredTokenUSerUtxos = userUtxos.filter(
    val => val.token && val.token.category === tokenId && val.token.amount >= mintPriceSats
  )
  const userBCHInput = filteredBCHUserUtxos[0];
  const userTokenInput = filteredTokenUSerUtxos[0];
  if (!userBCHInput) {
    alert("No suitable utxos found for network fee.");
    cleanupFailedMint();
    throw ("No suitable utxos found for network fee.");
  }
  if (!userTokenInput) {
    alert("No suitable utxos found with enough wrapped tokens. Consilidate?");
    cleanupFailedMint();
    throw ("No suitable utxos found with enough wrapped tokens.");
  }

  // Get the minting utxo to use from the api endpoint
  const getAvailableMintUtxo = async () => {
    const contractUtxos = await contract.getUtxos();
    console.log("Contract utxos", contractUtxos);

    let mintUtxo = contractUtxos.find(
      utxo => utxo.token != undefined && utxo.token.category == tokenId
      // contract needs to have enough wrapped satoshis
      && utxo.satoshis >= mintPriceSats
    );
    return mintUtxo;
  };
  const selectedMintUtxo = await getAvailableMintUtxo();
  if (selectedMintUtxo === undefined) {
    alert("Did not find wrapping contract");
    cleanupFailedMint();
    throw ("Did not find wrapping contract");
  }
  // create minting transaction
  const newContractBCHBalance = selectedMintUtxo.satoshis - BigInt(mintPriceSats);
  const newContractTokenBalance = selectedMintUtxo.token.amount + BigInt(mintPriceSats);
  const tokenDetailsContract = {
    amount: newContractTokenBalance,
    category: tokenId,
  };
  const changeBCHAmount = userBCHInput.satoshis + userTokenInput.satoshis + BigInt(mintPriceSats) - 2000n;
  const changeTokenAmount = userTokenInput.token.amount - BigInt(mintPriceSats);

  // empty usersig
  const userSig = new SignatureTemplate(Uint8Array.from(Array(32)));

  function toTokenAddress(address) {
    const addressInfo = decodeCashAddress(address);
    const pkhPayoutBin = addressInfo.payload;
    const prefix = network === "mainnet" ? 'bitcoincash' : 'bchtest';
    const tokenAddress = encodeCashAddress(prefix, "p2pkhWithTokens", pkhPayoutBin);
    return tokenAddress;
  }

  console.log("new contract balance", newContractBCHBalance, newContractTokenBalance);

  const transaction = contract.functions.wrapOrUnwrap()
    .from(selectedMintUtxo)
    .fromP2PKH(userBCHInput, userSig)
    .fromP2PKH(userTokenInput, userSig)
    .to(contract.tokenAddress, newContractBCHBalance, tokenDetailsContract)
    .withoutChange()
    .withTime(0);
  if (changeBCHAmount > 1000n) transaction.to(userAddress, changeBCHAmount);
  if (changeTokenAmount > 0n) transaction.to(toTokenAddress(userAddress), 1000n, {
    amount: changeTokenAmount,
    category: tokenId,
  })

  try {
    const rawTransactionHex = await transaction.build();
    const decodedTransaction = decodeTransaction(hexToBin(rawTransactionHex));
    console.log("Raw tx", rawTransactionHex);
    if (typeof decodedTransaction === "string") {
      alert("No suitable utxos found for unwrapping.");
      throw ("No suitable utxos found for unwrapping.");
    }
    decodedTransaction.inputs[1].unlockingBytecode = Uint8Array.from([]);
    decodedTransaction.inputs[2].unlockingBytecode = Uint8Array.from([]);

    // construct new transaction object for SourceOutputs, for stringify & not to mutate current network provider
    const listSourceOutputs = [{
      ...decodedTransaction.inputs[0],
      lockingBytecode: (cashAddressToLockingBytecode(contract.tokenAddress)).bytecode,
      valueSatoshis: BigInt(selectedMintUtxo.satoshis),
      token: selectedMintUtxo.token && {
        ...selectedMintUtxo.token,
        category: hexToBin(selectedMintUtxo.token.category),
      },
      contract: {
        abiFunction: transaction.abiFunction,
        redeemScript: contract.redeemScript,
        artifact: contract.artifact,
      }
    }, {
      ...decodedTransaction.inputs[1],
      lockingBytecode: (cashAddressToLockingBytecode(userAddress)).bytecode,
      valueSatoshis: BigInt(userBCHInput.satoshis),
    },
    {
      ...decodedTransaction.inputs[2],
      lockingBytecode: (cashAddressToLockingBytecode(userAddress)).bytecode,
      valueSatoshis: BigInt(userTokenInput.satoshis),
      token: userTokenInput.token && {
        ...userTokenInput.token,
        category: hexToBin(userTokenInput.token.category)
      }
    }];

    const wcTransactionObj = {
      transaction: decodedTransaction,
      sourceOutputs: listSourceOutputs,
      broadcast: true,
      userPrompt: "Unwrap BCH"
    };

    console.log(wcTransactionObj);
    setTimeout(() => alert('Approve the minting transaction in Cashonize'), 100);
    const signResult = await signTransaction(wcTransactionObj);
    console.log(signResult);
    if (signResult) {
      alert(`Unwrap succesful! txid: ${signResult.signedTransactionHash}`);
      console.log(`Unwrap succesful! txid: ${signResult.signedTransactionHash}`);
      unwrapButton.textContent = "Unwrap";

    } else {
      alert('Minting transaction cancelled');
      console.log('Minting transaction cancelled');
      unwrapButton.textContent = "Unrap";
    }
    unwrapButton.onclick = unwrapBCH;
  } catch (error) {
    alert(error);
    console.log(error);
    cleanupFailedMint();
  }
}

const fetchInfoNinja = async (nftNumber) => {
  try {
    const promiseNinjaInfo = await fetch(urlApiServer + "/nfts/" + nftNumber);
    let respNinjaInfo = await promiseNinjaInfo.json();
    // If empty response, refetch API server after 0.5 sec
    if (Object.keys(respNinjaInfo).length === 0) {
      setTimeout(async () => { respNinjaInfo = await fetchInfoNinja(nftNumber), 500; });
    } else {
      document.getElementById("imageMint").src = urlApiServer + "/images/" + nftNumber;
      document.getElementById("textImageMint").textContent = respNinjaInfo.name;
    }
  } catch (error) { console.log(error); }
};

async function signTransaction(options) {
  try {
    const result = await signClient.request({
      chainId: connectedChain,
      topic: session.topic,
      request: {
        method: "bch_signTransaction",
        params: JSON.parse(stringify(options)),
      },
    });

    return result;
  } catch (error) {
    return undefined;
  }
}

async function getUserAddress() {
  try {
    const result = await signClient.request({
      chainId: connectedChain,
      topic: session.topic,
      request: {
        method: "bch_getAddresses",
        params: {},
      },
    });
    return result[0];
  } catch (error) {
    return undefined;
  }
};
